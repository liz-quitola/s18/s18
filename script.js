// console.log('hello')

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	//methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled '+ target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health -= this.attack;
		
		 if (target.health <= 5){
		 	target.faint();
		 }
		}

	this.faint = function() {
		console.log(target.name + " fainted.");
	}
}

let raichu = new Pokemon ('Raichu', 10);
let balbasaur = new Pokemon('Balbasaur', 20);

raichu.tackle(balbasaur)
raichu.tackle(balbasaur)
raichu.tackle(balbasaur)
raichu.tackle(balbasaur)
raichu.tackle(balbasaur)
raichu.tackle(balbasaur)
